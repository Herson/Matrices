let rows = document.querySelector("tbody").children
let matrix = []
for (var i = 0; i < rows.length; i++) {
    matrix.push(rows[i].children)
}
function paintAll() {
    erase();
    for (let i=0;i <matrix.length; i++ ) { // afegir codi
        for (let j =0;j < matrix[0].length;j++ ) { // afegir codi
            matrix[i][j].style.backgroundColor = "red";
        }
    }
}

function erase() {
    for (let i=0;i <matrix.length; i++ ) { // afegir codi
        for (let j =0;j < matrix[0].length;j++ ) { // afegir codi
            matrix[i][j].style.backgroundColor = "white";
        }
    }
}

function paintRightHalf() {
    erase();
    for (let i=0;i <matrix.length; i++) { // afegir codi
        for (let j =0;j < matrix[0].length;j++) { // afegir codi
            if(j > 2){
                matrix[i][j].style.backgroundColor = "red";
            }
        }
    }
}

function paintLeftHalf() {
    erase();

    for (let i=0;i <matrix.length; i++) { // afegir codi
        for (let j =0;j < matrix[0].length/2;j++) { // afegir codi
            matrix[i][j].style.backgroundColor = "red";
        }
    }

}

function paintUpperHalf() {

    erase();
    for (let i=0;i <matrix.length; i++) { // afegir codi
        for (let j =0;j < matrix[0].length;j++) { // afegir codi
            if(i < 3){
                matrix[i][j].style.backgroundColor = "red";
            }
        }
    }

}

function paintLowerTriangle() {
    erase();
    var controlador =0;
    for (let i=0;i <matrix.length; i++) { // afegir codi
        controlador =0;
        for (let j =0;j < matrix[0].length;j++) { // afegir codi
            if(i > controlador ){
                matrix[i][j].style.backgroundColor = "red";
                controlador++;
            }
           
        }
    }

}

function paintUpperTriangle() {
    erase();
   
    for (let i=0;i <matrix.length; i++) { // afegir codi
        for (let j =0;j < matrix[0].length;j++){ // afegir codi
            if(i <= j ){
                matrix[i][j].style.backgroundColor = "red";
               
            }
        }
    }

}

function paintPerimeter() {
    erase();
    for (let i=0;i <matrix.length; i++) {
        for (let j =0;j < matrix[0].length;j++) {
            // afegir codi
            if((j ==0 || j == matrix[0].length-1) ||(i==0 || i ==matrix[0].length) ){
                matrix[i][j].style.backgroundColor = "red";
            }
            
        }
    }
}

function paintCheckerboard() {
    erase();
    for (let i=0;i <matrix.length; i++) { // afegir codi
        for (let j =0;j < matrix[0].length;j++) { // afegir codi
           if((i %2==0 && j %2==0) || ( i%2==1 && j%2==1)) {
            matrix[i][j].style.backgroundColor = "red";
           }
        }
    }
}
function paintCheckerboard2() {
    erase();
    for (let i=0;i <matrix.length; i++) { // afegir codi
        for (let j =0;j < matrix[0].length;j++) { // afegir codi
           if((i %2==0 || j %2==0) && ( i%2==1 || j%2==1)) {
            matrix[i][j].style.backgroundColor = "red";
           }
        }
    }
}
